var myApp = angular.module('myApp',
  ['ngRoute'])



myApp.run(['$rootScope', '$location',
  function($rootScope, $location) {
    $rootScope.$on('$routeChangeError',
      function(event, next, previous, error) {
        if (error) {
          console.log(error);
          $location.path('/main');
        } // error
      }); //event info
  }]); //run

myApp.config(['$routeProvider', function($routeProvider) {
  $routeProvider.
    when('/main', {
      templateUrl: 'views/portfolio-list.html',
      controller: 'PortfolioListCtrl'
    }).
    otherwise({
      redirectTo: '/main'
    });
}]);